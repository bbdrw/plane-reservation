﻿namespace Plane_Reservation
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn0 = new System.Windows.Forms.Button();
            this.btn2 = new System.Windows.Forms.Button();
            this.btn1 = new System.Windows.Forms.Button();
            this.btn4 = new System.Windows.Forms.Button();
            this.btn5 = new System.Windows.Forms.Button();
            this.btn3 = new System.Windows.Forms.Button();
            this.btn7 = new System.Windows.Forms.Button();
            this.btn8 = new System.Windows.Forms.Button();
            this.btn6 = new System.Windows.Forms.Button();
            this.btn10 = new System.Windows.Forms.Button();
            this.btn11 = new System.Windows.Forms.Button();
            this.btn9 = new System.Windows.Forms.Button();
            this.btn13 = new System.Windows.Forms.Button();
            this.btn14 = new System.Windows.Forms.Button();
            this.btn12 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnAddWait = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnBook = new System.Windows.Forms.Button();
            this.txtStatus = new System.Windows.Forms.TextBox();
            this.btnStatus = new System.Windows.Forms.Button();
            this.planeColumn = new System.Windows.Forms.ListBox();
            this.planeRow = new System.Windows.Forms.ListBox();
            this.btnShowAll = new System.Windows.Forms.Button();
            this.rchTxtSeats = new System.Windows.Forms.RichTextBox();
            this.btnShowWait = new System.Windows.Forms.Button();
            this.showWaiting = new System.Windows.Forms.RichTextBox();
            this.btnFillAll = new System.Windows.Forms.Button();
            this.seatChart = new System.Windows.Forms.Panel();
            this.groupBox1.SuspendLayout();
            this.seatChart.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn0
            // 
            this.btn0.Location = new System.Drawing.Point(38, 37);
            this.btn0.Name = "btn0";
            this.btn0.Size = new System.Drawing.Size(71, 65);
            this.btn0.TabIndex = 0;
            this.btn0.UseVisualStyleBackColor = true;
            // 
            // btn2
            // 
            this.btn2.Location = new System.Drawing.Point(192, 37);
            this.btn2.Name = "btn2";
            this.btn2.Size = new System.Drawing.Size(71, 65);
            this.btn2.TabIndex = 1;
            this.btn2.UseVisualStyleBackColor = true;
            // 
            // btn1
            // 
            this.btn1.Location = new System.Drawing.Point(115, 37);
            this.btn1.Name = "btn1";
            this.btn1.Size = new System.Drawing.Size(71, 65);
            this.btn1.TabIndex = 2;
            this.btn1.UseVisualStyleBackColor = true;
            // 
            // btn4
            // 
            this.btn4.Location = new System.Drawing.Point(115, 108);
            this.btn4.Name = "btn4";
            this.btn4.Size = new System.Drawing.Size(71, 65);
            this.btn4.TabIndex = 5;
            this.btn4.UseVisualStyleBackColor = true;
            // 
            // btn5
            // 
            this.btn5.Location = new System.Drawing.Point(192, 108);
            this.btn5.Name = "btn5";
            this.btn5.Size = new System.Drawing.Size(71, 65);
            this.btn5.TabIndex = 4;
            this.btn5.UseVisualStyleBackColor = true;
            // 
            // btn3
            // 
            this.btn3.Location = new System.Drawing.Point(38, 108);
            this.btn3.Name = "btn3";
            this.btn3.Size = new System.Drawing.Size(71, 65);
            this.btn3.TabIndex = 3;
            this.btn3.UseVisualStyleBackColor = true;
            // 
            // btn7
            // 
            this.btn7.Location = new System.Drawing.Point(115, 179);
            this.btn7.Name = "btn7";
            this.btn7.Size = new System.Drawing.Size(71, 65);
            this.btn7.TabIndex = 8;
            this.btn7.UseVisualStyleBackColor = true;
            // 
            // btn8
            // 
            this.btn8.Location = new System.Drawing.Point(192, 179);
            this.btn8.Name = "btn8";
            this.btn8.Size = new System.Drawing.Size(71, 65);
            this.btn8.TabIndex = 7;
            this.btn8.UseVisualStyleBackColor = true;
            // 
            // btn6
            // 
            this.btn6.Location = new System.Drawing.Point(38, 179);
            this.btn6.Name = "btn6";
            this.btn6.Size = new System.Drawing.Size(71, 65);
            this.btn6.TabIndex = 6;
            this.btn6.UseVisualStyleBackColor = true;
            // 
            // btn10
            // 
            this.btn10.Location = new System.Drawing.Point(115, 250);
            this.btn10.Name = "btn10";
            this.btn10.Size = new System.Drawing.Size(71, 65);
            this.btn10.TabIndex = 11;
            this.btn10.UseVisualStyleBackColor = true;
            // 
            // btn11
            // 
            this.btn11.Location = new System.Drawing.Point(192, 250);
            this.btn11.Name = "btn11";
            this.btn11.Size = new System.Drawing.Size(71, 65);
            this.btn11.TabIndex = 10;
            this.btn11.UseVisualStyleBackColor = true;
            // 
            // btn9
            // 
            this.btn9.Location = new System.Drawing.Point(38, 250);
            this.btn9.Name = "btn9";
            this.btn9.Size = new System.Drawing.Size(71, 65);
            this.btn9.TabIndex = 9;
            this.btn9.UseVisualStyleBackColor = true;
            // 
            // btn13
            // 
            this.btn13.Location = new System.Drawing.Point(115, 321);
            this.btn13.Name = "btn13";
            this.btn13.Size = new System.Drawing.Size(71, 65);
            this.btn13.TabIndex = 14;
            this.btn13.UseVisualStyleBackColor = true;
            // 
            // btn14
            // 
            this.btn14.Location = new System.Drawing.Point(192, 321);
            this.btn14.Name = "btn14";
            this.btn14.Size = new System.Drawing.Size(71, 65);
            this.btn14.TabIndex = 13;
            this.btn14.UseVisualStyleBackColor = true;
            // 
            // btn12
            // 
            this.btn12.Location = new System.Drawing.Point(38, 321);
            this.btn12.Name = "btn12";
            this.btn12.Size = new System.Drawing.Size(71, 65);
            this.btn12.TabIndex = 12;
            this.btn12.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.label1.Location = new System.Drawing.Point(62, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(27, 29);
            this.label1.TabIndex = 15;
            this.label1.Text = "0";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.label2.Location = new System.Drawing.Point(138, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(27, 29);
            this.label2.TabIndex = 16;
            this.label2.Text = "1";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.label3.Location = new System.Drawing.Point(217, 6);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(27, 29);
            this.label3.TabIndex = 17;
            this.label3.Text = "2";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.label4.Location = new System.Drawing.Point(2, 50);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(30, 29);
            this.label4.TabIndex = 18;
            this.label4.Text = "A";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.label5.Location = new System.Drawing.Point(2, 121);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(30, 29);
            this.label5.TabIndex = 19;
            this.label5.Text = "B";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.label6.Location = new System.Drawing.Point(2, 263);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(31, 29);
            this.label6.TabIndex = 21;
            this.label6.Text = "D";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.label7.Location = new System.Drawing.Point(2, 192);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(31, 29);
            this.label7.TabIndex = 20;
            this.label7.Text = "C";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.label8.Location = new System.Drawing.Point(2, 334);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(30, 29);
            this.label8.TabIndex = 22;
            this.label8.Text = "E";
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(141, 44);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(302, 22);
            this.txtName.TabIndex = 23;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(33, 49);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(49, 17);
            this.label9.TabIndex = 24;
            this.label9.Text = "Name:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnAddWait);
            this.groupBox1.Controls.Add(this.btnCancel);
            this.groupBox1.Controls.Add(this.btnBook);
            this.groupBox1.Controls.Add(this.txtStatus);
            this.groupBox1.Controls.Add(this.btnStatus);
            this.groupBox1.Controls.Add(this.txtName);
            this.groupBox1.Controls.Add(this.planeColumn);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.planeRow);
            this.groupBox1.Location = new System.Drawing.Point(354, 53);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(550, 336);
            this.groupBox1.TabIndex = 26;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Booking and Cancellation";
            // 
            // btnAddWait
            // 
            this.btnAddWait.Location = new System.Drawing.Point(36, 287);
            this.btnAddWait.Name = "btnAddWait";
            this.btnAddWait.Size = new System.Drawing.Size(187, 32);
            this.btnAddWait.TabIndex = 30;
            this.btnAddWait.Text = "Add To Waiting List";
            this.btnAddWait.UseVisualStyleBackColor = true;
            this.btnAddWait.Click += new System.EventHandler(this.btnAddWait_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(141, 233);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 32);
            this.btnCancel.TabIndex = 29;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnBook
            // 
            this.btnBook.Location = new System.Drawing.Point(36, 233);
            this.btnBook.Name = "btnBook";
            this.btnBook.Size = new System.Drawing.Size(75, 32);
            this.btnBook.TabIndex = 28;
            this.btnBook.Text = "Book";
            this.btnBook.UseVisualStyleBackColor = true;
            this.btnBook.Click += new System.EventHandler(this.btnBook_Click);
            // 
            // txtStatus
            // 
            this.txtStatus.ForeColor = System.Drawing.SystemColors.MenuText;
            this.txtStatus.Location = new System.Drawing.Point(264, 129);
            this.txtStatus.Name = "txtStatus";
            this.txtStatus.ReadOnly = true;
            this.txtStatus.Size = new System.Drawing.Size(149, 22);
            this.txtStatus.TabIndex = 27;
            // 
            // btnStatus
            // 
            this.btnStatus.Location = new System.Drawing.Point(264, 81);
            this.btnStatus.Name = "btnStatus";
            this.btnStatus.Size = new System.Drawing.Size(75, 32);
            this.btnStatus.TabIndex = 26;
            this.btnStatus.Text = "Status";
            this.btnStatus.UseVisualStyleBackColor = true;
            this.btnStatus.Click += new System.EventHandler(this.btnStatus_Click);
            // 
            // planeColumn
            // 
            this.planeColumn.FormattingEnabled = true;
            this.planeColumn.ItemHeight = 16;
            this.planeColumn.Location = new System.Drawing.Point(141, 81);
            this.planeColumn.Name = "planeColumn";
            this.planeColumn.Size = new System.Drawing.Size(82, 132);
            this.planeColumn.TabIndex = 25;
            this.planeColumn.SelectedIndexChanged += new System.EventHandler(this.planeColumn_SelectedIndexChanged);
            // 
            // planeRow
            // 
            this.planeRow.FormattingEnabled = true;
            this.planeRow.ItemHeight = 16;
            this.planeRow.Location = new System.Drawing.Point(36, 81);
            this.planeRow.Name = "planeRow";
            this.planeRow.Size = new System.Drawing.Size(82, 132);
            this.planeRow.TabIndex = 0;
            this.planeRow.SelectedIndexChanged += new System.EventHandler(this.planeRow_SelectedIndexChanged);
            // 
            // btnShowAll
            // 
            this.btnShowAll.Location = new System.Drawing.Point(45, 408);
            this.btnShowAll.Name = "btnShowAll";
            this.btnShowAll.Size = new System.Drawing.Size(225, 32);
            this.btnShowAll.TabIndex = 31;
            this.btnShowAll.Text = "Show All";
            this.btnShowAll.UseVisualStyleBackColor = true;
            this.btnShowAll.Click += new System.EventHandler(this.btnShowAll_Click);
            // 
            // rchTxtSeats
            // 
            this.rchTxtSeats.Location = new System.Drawing.Point(45, 446);
            this.rchTxtSeats.Name = "rchTxtSeats";
            this.rchTxtSeats.ReadOnly = true;
            this.rchTxtSeats.Size = new System.Drawing.Size(225, 305);
            this.rchTxtSeats.TabIndex = 32;
            this.rchTxtSeats.Text = "";
            // 
            // btnShowWait
            // 
            this.btnShowWait.Location = new System.Drawing.Point(354, 408);
            this.btnShowWait.Name = "btnShowWait";
            this.btnShowWait.Size = new System.Drawing.Size(308, 32);
            this.btnShowWait.TabIndex = 33;
            this.btnShowWait.Text = "Show Waiting List";
            this.btnShowWait.UseVisualStyleBackColor = true;
            this.btnShowWait.Click += new System.EventHandler(this.btnShowWait_Click);
            // 
            // showWaiting
            // 
            this.showWaiting.Location = new System.Drawing.Point(354, 446);
            this.showWaiting.Name = "showWaiting";
            this.showWaiting.ReadOnly = true;
            this.showWaiting.Size = new System.Drawing.Size(308, 305);
            this.showWaiting.TabIndex = 34;
            this.showWaiting.Text = "";
            // 
            // btnFillAll
            // 
            this.btnFillAll.Location = new System.Drawing.Point(787, 408);
            this.btnFillAll.Name = "btnFillAll";
            this.btnFillAll.Size = new System.Drawing.Size(117, 105);
            this.btnFillAll.TabIndex = 35;
            this.btnFillAll.Text = "Fill All (Debug)";
            this.btnFillAll.UseVisualStyleBackColor = true;
            this.btnFillAll.Click += new System.EventHandler(this.btnFillAll_Click_1);
            // 
            // seatChart
            // 
            this.seatChart.Controls.Add(this.label8);
            this.seatChart.Controls.Add(this.label6);
            this.seatChart.Controls.Add(this.label7);
            this.seatChart.Controls.Add(this.label5);
            this.seatChart.Controls.Add(this.label4);
            this.seatChart.Controls.Add(this.label3);
            this.seatChart.Controls.Add(this.label2);
            this.seatChart.Controls.Add(this.btn13);
            this.seatChart.Controls.Add(this.btn14);
            this.seatChart.Controls.Add(this.btn12);
            this.seatChart.Controls.Add(this.btn10);
            this.seatChart.Controls.Add(this.btn11);
            this.seatChart.Controls.Add(this.btn9);
            this.seatChart.Controls.Add(this.btn7);
            this.seatChart.Controls.Add(this.btn8);
            this.seatChart.Controls.Add(this.btn6);
            this.seatChart.Controls.Add(this.btn4);
            this.seatChart.Controls.Add(this.btn5);
            this.seatChart.Controls.Add(this.btn3);
            this.seatChart.Controls.Add(this.btn1);
            this.seatChart.Controls.Add(this.btn2);
            this.seatChart.Controls.Add(this.btn0);
            this.seatChart.Controls.Add(this.label1);
            this.seatChart.Location = new System.Drawing.Point(7, 3);
            this.seatChart.Name = "seatChart";
            this.seatChart.Size = new System.Drawing.Size(297, 395);
            this.seatChart.TabIndex = 36;
            this.seatChart.Visible = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 778);
            this.Controls.Add(this.seatChart);
            this.Controls.Add(this.btnFillAll);
            this.Controls.Add(this.showWaiting);
            this.Controls.Add(this.btnShowWait);
            this.Controls.Add(this.rchTxtSeats);
            this.Controls.Add(this.btnShowAll);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "RWingAssignment1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.seatChart.ResumeLayout(false);
            this.seatChart.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn0;
        private System.Windows.Forms.Button btn2;
        private System.Windows.Forms.Button btn1;
        private System.Windows.Forms.Button btn4;
        private System.Windows.Forms.Button btn5;
        private System.Windows.Forms.Button btn3;
        private System.Windows.Forms.Button btn7;
        private System.Windows.Forms.Button btn8;
        private System.Windows.Forms.Button btn6;
        private System.Windows.Forms.Button btn10;
        private System.Windows.Forms.Button btn11;
        private System.Windows.Forms.Button btn9;
        private System.Windows.Forms.Button btn13;
        private System.Windows.Forms.Button btn14;
        private System.Windows.Forms.Button btn12;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnAddWait;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnBook;
        private System.Windows.Forms.TextBox txtStatus;
        private System.Windows.Forms.Button btnStatus;
        private System.Windows.Forms.ListBox planeColumn;
        private System.Windows.Forms.ListBox planeRow;
        private System.Windows.Forms.Button btnShowAll;
        private System.Windows.Forms.RichTextBox rchTxtSeats;
        private System.Windows.Forms.Button btnShowWait;
        private System.Windows.Forms.RichTextBox showWaiting;
        private System.Windows.Forms.Button btnFillAll;
        private System.Windows.Forms.Panel seatChart;
    }
}

