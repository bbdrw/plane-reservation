﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Plane_Reservation
{

    public partial class Form1 : Form
    {

        //Set int i value to 15

        int i = 15;

        //Define a control object called "btnControl"
        //Define a public multi-dimensional array  called planeSeat
        //Define a public multi-dimensional array  called waitingList
        //Set the string clientName value to ""
        //Set the Boolean isWaitingShown value to false
        //Set the Boolean isStatusShowing value to false

        Control btnControl;
        public String[,] planeSeat;
        public String[,] waitingList;
        String clientName = "";
        Boolean isWaitingShown = false;
        Boolean isStatusShowing = false;

        public Form1()
        {

            //Call the function InitializeComponenet()

            InitializeComponent();

            //Set planeSeat value to a new mulit-dimensional array with size i,6
            //Set waitingList value to a new multi-dimensional array with size 10,2

            planeSeat = new String[i, 6];
            waitingList = new String[10, 2];

            //Set int d value to 0

            int d = 0;

            //While the value od is less than 10 run the below

            while (d < 10)
            {

                //Set the attribute at position d,0 in the waitingList array to None
                //Set the attribute t position d,1 int he waitingList array to false

                waitingList[d, 0] = "None";
                waitingList[d, 1] = "false";

                //Increment d by 1

                d++;
            }

            //Call the SetPlaneInfo function

            SetPlaneInfo();

            //Set the backcolor of the status txt box to GhostWhite

            txtStatus.BackColor = Color.GhostWhite;

        }

        //Function definition for SetPlaneInfo

        public void SetPlaneInfo()
        {
            //Set int r value to 0
            //Set int c value to 0
            //Set int btnCount value to 0

            int r = 0;
            int c = 0;
            int btnCount = 0;
            
            //Set the string buttonName to btn

            String buttonName = "btn";

            //Set int y value to 0
            //Set Char myRow value to 'A'

            int y = 0;
            Char myRow = 'A';

            //while the value of y is less than 15 run the below code

            while (y < 15)
            {

                //Set buttonName value to "btn"
                //Set buttonName value to itslef plus the value of btnCount

                buttonName = "btn";
                buttonName = buttonName + btnCount;

                //Set the attribute in position y,0 is equal to buttonName value
                //Set the attribute in position y,1 is equal to variable r converted to a string value
                //Set the attribute in position y,2 is equal to variable c converted to a string value
                //Set the attribute in position y,3 is equal to false
                //Set the attribute in position y,4 is equal to None
                //Set the attribute in position y,5 is equal to myRow value plus variable c converted to a stirng value

                planeSeat[y, 0] = buttonName;
                planeSeat[y, 1] = r.ToString();
                planeSeat[y, 2] = c.ToString();
                planeSeat[y, 3] = "false";
                planeSeat[y, 4] = "None";
                planeSeat[y, 5] = myRow + c.ToString();

                //btnControl has a value of the form controller that has a id name of the attribute
                //at position p,0 in the planeSeat array
                //Reference the button and change the backcolor of the button to light green

                btnControl = this.Controls.Find(planeSeat[y, 0], true)[0];
                this.btnControl.BackColor = Color.LightGreen;

                //Increment c by 1

                c++;

                //if the value of c is greater than or equal to 3
                //Set the value of c to 0
                //Increment myRow by 1
                //Increment r by 1

                if (c >= 3)
                {
                    c = 0;
                    myRow++;
                    r++;
                }

                //Increment btnCount by 1
                //Increment y by 1

                btnCount++;
                y++;

            }

            //Set int f value to 0
            //Set Char charInc value to 'A'

            int f = 0;
            char charInc = 'A';

            //While the value of f is less than 5 run the below code

            while (f < 5)
            {

                //If the value of f is less than 3, Add a list box item to the planeColumn controller

                if (f < 3)
                {
                    planeColumn.Items.Add(f);
                }

                //Add a list box item to the planeRow controller
                //Increment charInc by 1
                //Increment f by 1

                planeRow.Items.Add(charInc);
                charInc++;

                f++;
            }




        }

        //Function definition for BookingValidation

        public Boolean BookingValidation()
        {
            //Set Boolean isWronge value to false
            //Set Boolean isFirst value to false
            //Set String errorMessage value to ""

            Boolean isWronge = false;
            Boolean isFirst = false;
            String errorMessage = "";

            //If the txtName value has a lenght of 0 OR txtName value is "" OR txtName value is null
            //OR txtName value is equal to " ", set the errorMessage to "you have not entered a valid name"
            //Set the value of isFirst to true
            //Set the value of isWronge to true

            if (txtName.Text.Length == 0 || txtName.Text == "" || txtName.Text == null || txtName.Text == " ")
            {
                errorMessage = "You have not entered a valid name.";
                isFirst = true;
                isWronge = true;
            }

            //If the selected row dows not equal to -1 AND the selected column does not equal -1 then
            //If the value of isStatusShowing is true then run the below code

            if (planeRow.SelectedIndex.ToString() == "-1" || planeColumn.SelectedIndex.ToString() == "-1")
            {

                //If the value of isFirst is equal to true, set the errorMessage value to 
                //"As well as you have not chosen a row or column for the seat. Please Try Again"

                if (isFirst)
                {
                    errorMessage += "As well as you have not chosen a row or column for the seat. Please Try Again";

                }

                //If the above statement is false then set the errorMessage value to 
                //"You have not chosen a row or column for the seat please try again!" and set
                //isWronge value to true

                else
                {
                    errorMessage = "You have not chosen a row or column for the seat please try again!";
                    isWronge = true;
                }
            }

            //If the value of isWronge is equal to true then display a message box displaying 
            //the value of errorMessage

            if (isWronge == true)
            {

                MessageBox.Show(errorMessage);
            }

            //return the boolean value of isWronge

            return isWronge;

        }

        //Function definition for CheckSeat

        public void CheckSeat()
        {
            //Set String seatRow value to the index from the listbox "planeRow"
            //Set String seatColumn value to the index from the listbox "planeRow"

            //Set String seatNumber value to seatRow value plus seatColumn value
            //Set String userMessage value to ""

            String seatRow = planeRow.SelectedIndex.ToString();
            String seatColumn = planeColumn.SelectedIndex.ToString();

            String lookupSeat = seatRow + seatColumn;
            String userMessage = "";

            //Set int i value to 0
            //Set int x value to 0
            //Set Boolean isTaken value to false

            int i = 0;
            int x = 0;
            Boolean isTaken = false;

            //While the value of i is less than 15 run the below code

            while (i < 15)
            {

                //Set the value for seatCheck to the attribute at position i,1 in the planeSeat array
                //plus the attribute at position i,2 in in the planeSeat array

                String seatCheck = planeSeat[i, 1] + planeSeat[i, 2];

                //If the seatCheck value equals the value of lookupSeat AND the attribute at
                //position i,3 in the planeSeat array is equal to false run the below code

                if (seatCheck.Equals(lookupSeat) && planeSeat[i, 3] == "false")
                {
                    //Set the value isTaken to true
                    //Set the attribute at position i,3 in the planeSeat array to true
                    //Set the attribute at position i,4 in the planeSeat array to the txtName textbox value
                    
                    isTaken = true;

                    planeSeat[i, 3] = "true";
                    planeSeat[i, 4] = txtName.Text;

                    //btnControl has a value of the form controller that has a id name of the attribute
                    //at position g,0 in the planeSeat array
                    //Reference the button and change the backcolor of the button to PaleVioletRed

                    btnControl = this.Controls.Find(planeSeat[i, 0], true)[0];
                    this.btnControl.BackColor = Color.PaleVioletRed;

                    //Set string customerName value to the value of txtName textbox value

                    String customerName = txtName.Text;

                    //Display a messagebox telling the user that the customer has been added to the seat
                    //at the attribute at position i,5 in the planeSeat array

                    MessageBox.Show(customerName + " has been added to seat: " + planeSeat[i, 5]);

                    //If the seatChart panel is invisible then call the funcgtion ShowSeatInfo
                    //If the value of isStatusShowing is true then call the function FinSeatStatus

                    if (seatChart.Visible == true)
                    {
                        ShowSeatInfo();

                        if (isStatusShowing)
                        {
                            FindSeatStatus();
                        }                        

                    }

                    //Call the clearSeatInfo function
                    ClearSeatInfo();

                    //Break out of the while loop

                    break;

                }

                //If the above statement is false AND seatCheck value equals the value of lookupSeat
                // AND the attribute at position i,3 in the planeSeat array is equal to true then 
                //set the value of userMessage to "This seat is taken"

                else if (seatCheck.Equals(lookupSeat) && planeSeat[i, 3] == "true")
                {
                    userMessage = "This seat is taken,";
                }

                //Increment i by 1

                i++;
            }

            //If the value of isTaken is equal to false run the below code

            if (isTaken == false)
            {

               //Set int seatTaken value to 0

                int seatTaken = 0;

                //Set the int l to 0, and as long as l is less than 15 run the below code and increment
                //l by 1

                for (int l = 0; l < 15; l++)
                {
                    //If the attribute at position l,3 is equal to true, increment seatTaken by 1

                    if (planeSeat[l, 3] == "true")
                    {
                        seatTaken++;
                    }

                }

                //If the value of seatTaken is equal to 15, set the userMessage to itself plus
                //the message that the rest of the seats have alos been taken
                //Call the function CheckWaitingList and pass the userMessage value to the function

                if (seatTaken == 15)
                {
                    userMessage = userMessage + "as well as all seats are taken!";
               
                    CheckWaitingList(userMessage);
                }

                //If the above statement is false, set the userMessage to itslef plus the message that
                //other seats are available sot he user should choose a available seat
                //Display a messagebox displaying the userMessage value

                else
                {
                    userMessage = userMessage + " but there are other seats available, please choose another seat!";
                    MessageBox.Show(userMessage);
                }

            }

        }

        //Function definition for CheckSeats

        public Boolean CheckSeats()
        {
            //Set Boolean isAvailable value to false
            //Set int z value to 0

            Boolean isAvailable = false;

            int z = 0;

            //While the value of z is less than 15 run the below code

           while (z < 15)
            {

                //If the attribute at position z,3 in the planeSeat array is equal to false
                //set isAvailable value to true
                //Break out of the while loop

                if (planeSeat[z, 3] == "false")
                {
                    isAvailable = true;
                  break;
             }

                //Increment z by 1

                z++;
            }

            
           //Return the Boolean value isAvailable

            return isAvailable;
        }

        //Function definition for CheckWaitingList, with passing in a Stirng parameter

        public void CheckWaitingList(String userMessage)
        {


            //Set Boolean variable isFree value to true
            //Set int a value to 0
            //Set int isFull value to 0

            Boolean isFree = true;
            int a = 0;
            int isFull = 0;

            //While the value of a is less than 10 run the below code

            while (a < 10)
            {

                //If the attribute at position a,1 in the waitingList array is equalt to false run
                //the below code

                if (waitingList[a, 1] == "false")
                {

                    //Set attribute at position a,1 in the waitingList array to true
                    //Set attribute at position a,0 in the waitingList array to the txtName textbow value
                    
                    waitingList[a, 1] = "true";
                    waitingList[a, 0] = txtName.Text;

                    //If the value of userMessage length is greater than 0 OR the value of userMessage
                    //does not equal "", display a messagebox telling the user that the customer at
                    //position a,0 in the waitingList array was added to the waitingList
                    //This if statement is for when all of the seats are taken and the "book" button is
                    //pressed

                    //If the userMessage is empty then display a messagebox telling the user that the customer 
                    //at position a,0 in the waitingList was added to the waiting list

                    if(userMessage.Length > 0 || userMessage != "")
                    {
                        MessageBox.Show(userMessage + " " + waitingList[a, 0] + " was added to waiting list");
                    }
                    else
                    {
                        MessageBox.Show(waitingList[a, 0] + " was added to waiting list");
                    }
                    
                    //If the value of isWaitingShown is equal to true then call the function
                    //DisplayWaitingList with passing the integer 1

                    if (isWaitingShown == true)
                    {
                        DisplayWaitingList(1);
                    }
                    break;

                }

                //If the above statement is false increment isFull by 1

                else
                {
                    isFull++;
                }

                //Increment a by 1

                a++;
            }

            //Call the ClearSeatInfo function

            ClearSeatInfo();

            //If the value of isFull is equal to 10 dispaly a messagebox telling the user that the
            //waiting list is full

            if (isFull == 10)
            {
                MessageBox.Show("Waiting List is Full!");
            }
        }

        //Function definition for FillAllSeats

        public void FillAllSeats()
        {
            //Set customerName value to Dylly D

            String customerName = "Dylly D";

            //Set int g to 0, as long as g value is less than 15, run the below code and then increment
            //g by 1

            for (int g = 0; g < 15; g++)
            {

                //If attribute at position g,3 in the planeSeat array has a value of false then
                //set the attribute at position g,3 in the planeSeat array to true
                //set the attribute at position g,4 in the planeSeat array to the customers' name

                //btnControl has a value of the form controller that has a id name of the attribute
                //at position g,0 in the planeSeat array
                //Reference the button and change the backcolor of the button to PaleVioletRed

                if (planeSeat[g, 3] == "false")
                {
                    planeSeat[g, 3] = "true";
                    planeSeat[g, 4] = customerName;

                    btnControl = this.Controls.Find(planeSeat[g, 0], true)[0];
                    this.btnControl.BackColor = Color.PaleVioletRed;

                }


            }

            //If the selected row dows not equal to -1 AND the selected column does not equal -1 then
            //If the value of isStatusShowing is true then call the function FindSeatStatus

            if (planeRow.SelectedIndex.ToString() != "-1" && planeColumn.SelectedIndex.ToString() != "-1")
            {
                if (isStatusShowing)
                {
                    FindSeatStatus();
                }
            }
        }

        //Function definition for DisplayWaitingList, has an int parameter 

        public void DisplayWaitingList(int myCheck)
        {

            //Set int u value to 0
            //Set showWaiting rich text box text to ""

            int u = 0;
            showWaiting.Text = "";

            //create int b with a value of 0, as long as the value of b is less than 10 run the below code
            //and then increment b by 1

            for (int b = 0; b < 10; b++)
            {

                //Set waitName string value to attribute at position b,0 in the waitingList array

                String waitName = waitingList[b, 0];

                //If the attribute at position b,1 in the waitingList array is equal to true, set
                //showWaiting text to itself plus the waitname value and \n
                //Increment u by 1

                if (waitingList[b, 1] == "true")
                {
                    showWaiting.Text += waitName + "\n";
                    u++;
                }

            }

            //If the value of u is equal to 0 set showWaiting text to ""
            
            //If the value of myCheck is equal to 1, display a messagebox telling the user there
            //are no customers on the waiting list and therefore is empty

            if (u == 0)
            {
                showWaiting.Text = "";
                if (myCheck == 1)
                {
                    MessageBox.Show("There are no entries on the waiting list");
                }

            }

        }

        //Declare the ClearSeatInfo function

        public void ClearSeatInfo()
        {

            //Set txtName tetxbox text to ""
            //Clear the planeRow listbox so the selected row is now reset
            //Clear the planeColumn listbox so the selected column is now reset

            txtName.Text = "";
            planeRow.ClearSelected();
            planeColumn.ClearSelected();

        }

        //Function definition for FindSeatStatus 

        public void FindSeatStatus()
        {
            //Set String seatRow value to the index from the listbox "planeRow"
            //Set String seatColumn value to the index from the listbox "planeRow"

            //Set String seatNumber value to seatRow value plus seatColumn value

            String seatRow = planeRow.SelectedIndex.ToString();
            String seatColumn = planeColumn.SelectedIndex.ToString();

            String seatNumber = seatRow + seatColumn;

            //Set the in l value to 0

            int l = 0;

            //While value l is less than 15 run the below code

            while (l < 15)
            {
                //Set the value of seatCheck to attribute at position l,1 in the planeSeat array 
                //plus attribute at position l,2 in the planeSeat array

                String seatCheck = planeSeat[l, 1] + planeSeat[l, 2];

                //If the value of seatCheck is equal to the value of seatNumber run the below code

                if (seatCheck.Equals(seatNumber))
                {

                    //If the attribute at position l,3 in the planeSeat array is equal to "false"
                    //set the value to txtStatus forcolor to dark green as well as set the txtStatus
                    //text box text to "Available"

                    //If the above statement is true set the forecolor of the txtStatus text to darkred
                    //as well as set the txtStatus text box to "Not Available"

                    if (planeSeat[l, 3] == "false")
                    {
                        txtStatus.ForeColor = Color.DarkGreen;
                        txtStatus.Text = "Available";
                    }
                    else
                    {
                        txtStatus.ForeColor = Color.DarkRed;
                        txtStatus.Text = "Not Available";
                    }

                }

                //Incrmenet l by 1

                l++;
            }

        }

        //Function definition for CancelUserSeat 

        public void CancelUserSeat()
        {
            //Set String seatRow value to the index from the listbox "planeRow"
            //Set String seatColumn value to the index from the listbox "planeRow"

            //Set String seatNumber value to seatRow value plus seatColumn value

            String seatRow = planeRow.SelectedIndex.ToString();
            String seatColumn = planeColumn.SelectedIndex.ToString();

            String seatNumber = seatRow + seatColumn;

            //Set int p value to 0
            //Set Boolean cancelSeat value to false

            int p = 0;
            Boolean cancelSeat = false;

            //While value p is less than 15 run the below code

            while (p < 15)
            {

                //Set the String seatCheck value to the attribute at position p,1 in the planeSeat
                //multi dimensional array plus the attribute at position p,2 in the planeSeat multi
                //dimensional array

                String seatCheck = planeSeat[p, 1] + planeSeat[p, 2];

                //If seatCheck value is equal to the seatNumber value AND the attribute at
                //position p,3 in the planeseat array is equal to true

                if (seatCheck.Equals(seatNumber) && planeSeat[p, 3] == "true")
                {
                    //set String "plainName" value to attribute at position p,4 int he planeSeat array
                    //attribute at position p,4 in the planeSeat array  is equal to "none"
                    //attribute at position p,3 in the planeSeat array  is equal to "false"

                    String plainName = planeSeat[p, 4];

                    planeSeat[p, 4] = "None";
                    planeSeat[p, 3] = "false";

                    //btnControl has a value of the form controller that has a id name of the attribute
                    //at position p,0 in the planeSeat array
                    //Reference the button and change the backcolor of the button to light green

                    btnControl = this.Controls.Find(planeSeat[p, 0], true)[0];
                    this.btnControl.BackColor = Color.LightGreen;

                    //Set cancel seat value to true

                    cancelSeat = true;

                    //Display a messagebox telling the user that the reservation made at attribute p,5 in
                    //the planeSeat array has been removed 

                    MessageBox.Show("The reservation at seat: " + planeSeat[p, 5] + " has been removed");

                    //If the value of seatChart.Visible is equal to true, call the function
                    //"ShowSeatInfo" to diplay the information of the seat

                    if (seatChart.Visible)
                    {
                        ShowSeatInfo();
                    }

                    //Break out of the while loop
                    break;

                }

                //If the above if statement is false AND seatCheck has a value that equals seatNumber
                //value AND attribute at position p,3 in the planeSeat array has avlue of "false"
                //display a messagebox telling the user that the seat is already empty

                else if (seatCheck.Equals(seatNumber) && planeSeat[p, 3] == "false")
                {
                    MessageBox.Show("This seat is already vacent!");
                }

                //Increment p by 1

                p++;
            }

            //If the value of cancelSeat is equal to true run the below code

            if (cancelSeat == true)
            {
                //Set q value to 0

                int q = 0;

                //While the value of q is less than 10 run the below code
                

                while (q < 10)
                {
                    //If attribute at position q,1 in the planeSeat array has a value of true then
                    //run the below code

                    if (waitingList[q, 1] == "true")
                    {
                        //Set the value at position p,4 in the planeSeat array to the attribute's value
                        //at position q,0 in the waitingList array

                        //Set the attribute value at position p,3 in the planeSeat array to true

                        planeSeat[p, 4] = waitingList[q, 0];
                        planeSeat[p, 3] = "true";

                        //Set the attribute value at position q,0 in the waitingList array to none
                        //Set the attribute value at position q,1 in the waitingList array to false

                        waitingList[q, 0] = "None";
                        waitingList[q, 1] = "false";

                        //btnControl has a value of the form controller that has a id name of the attribute
                        //at position p,0 in the planeSeat array
                        //Reference the button and change the backcolor of the button to PaleVioletRed

                        btnControl = this.Controls.Find(planeSeat[p, 0], true)[0];
                        this.btnControl.BackColor = Color.PaleVioletRed;

                        //Display a messagebox telling the user that the customer at position p,4 in the planeSeat array has been removed 
                        //from the the seat name at position p,5 in the planeSeat array

                        MessageBox.Show(planeSeat[p, 4] + " has been removed from the waiting list and added to seat: " + planeSeat[p, 5]);
                        
                        //If the length of showWaiting tetxbox is greater than 0 OR showWaiting has a value that is not null
                        //Call the function DisplayWaitingList pasisng in the int value 1
                                                
                        if (showWaiting.Text.Length > 0 || showWaiting.Text != null)
                        {
                            DisplayWaitingList(1);
                        }

                        if (seatChart.Visible)
                        {
                            ShowSeatInfo();
                        }

                        //Break out of the while loop

                        break;
                    }

                    //Increment q by 1

                    q++;
                }
            }

           //Call the ClearSeatInfo function

            ClearSeatInfo();

        }

        //Function definition for ShowSeatInfo 

        public void ShowSeatInfo()
        {
            //Set the rchTxtSeats text to ""

            rchTxtSeats.Text = "";


            //set the integer value of x to 0
            //set the integer value of myCount to 0
            int x = 0;
            int myCount = 0;

            //while x has a value that is less than 15
            //If the plainSeat in dimension 1 at element x, and dimension 2 at 3 has a stirng value of true
            //Set the rchTxtSeats value to equal itself plus the seatPosition and seatCustomerName
            //Increment myCount by 1

            //Increment x by 1

            while (x < 15)
            {
                if (planeSeat[x, 3] == "true")
                {
                    rchTxtSeats.Text += planeSeat[x, 5] + "     |     " + planeSeat[x, 4] + "\n";
                    myCount++;
                }
                x++;
            }

        }

        //Call a onClick function when the button "btnBook" gets pressed

        private void btnBook_Click(object sender, EventArgs e)
        {
            //Set the boolean "isNotValid" to the value of the returned value from the function "Booking Validation"
            
            Boolean isNotValid = BookingValidation();

            //If the value of isNotValid is false then call the function CheckSeat to seee if the seat
            //is available to book

            if (!isNotValid)
            {
                CheckSeat();
            }

        }

        //Call a onClick function when the button "btnFillAll" gets pressed

        private void btnFillAll_Click_1(object sender, EventArgs e)
        {
            //Call the function "FillAllSeats" to set the same user to each seat. This button
            //is used as a debug solution to test outcomes of the form

            FillAllSeats();

            //If the panel seatChart is visible, call the function ShowSeatInfo

            if (seatChart.Visible == true)
            {
                ShowSeatInfo();
            }

        }

        //Call a onClick function when the button "btnShowWait" gets pressed

        private void btnShowWait_Click(object sender, EventArgs e)
        {
            //If Boolean isWaitingShown has a value of false, call the function "DisplayWaitingList"
            //to display the waiting list to the user and pass in an int value of 1. Also set
            //isWaitingShown value to true

            //If isWaitingShown has a vlaue of true, set showWaiting textbox to be empty or ""
            //Set isWaitingShown to false

            if (isWaitingShown == false)
            {
                DisplayWaitingList(1);
                isWaitingShown = true;
            }
            else
            {
                showWaiting.Text = "";
                isWaitingShown = false;
            }


        }

        //Call a onClick function when the button "btnAddWait" gets pressed

        private void btnAddWait_Click(object sender, EventArgs e)
        {
            //Set the Boolean value "isFull" to the returned value from the function "CheckSeats()" call
        
            Boolean isFull = CheckSeats();

            //If isFull has a value of true, display a messagebox telling the user
            //that there are seats that are available and to choose a seat(If the user
            //tried to add there name to the waiting list but there are seats available)

            //If TextBox "txtName" has a length of zero or "txtName" has a value of null or
            //if "txtName" has a value of "", display a message telling the user that have
            //not filled in a name and to try again

            //If both if statements are false then call the function "CheckWaitingList" to see
            //if there are avaiable spots ont he waiting list for the user
            
            if (isFull)
            {
                MessageBox.Show("There are seats available, please pick a seat!");
            }
            else if (txtName.Text.Length == 0 || txtName.Text == null || txtName.Text == "")
            {
                MessageBox.Show("You have not entered anything for a name, please try again!");
            }
            else
            {
                CheckWaitingList("");
            }

        }

        //Call a onClick function when the button "btnStatus" gets pressed

        private void btnStatus_Click(object sender, EventArgs e)
        {
            //If the planeRow listbox or planeColumn listBox have a value of -1 (nothing has been selected)
            //Then display a messagebox telling the user to choose a row and column

            //If the user has selected a valid seat check the Boolean value "isStatusShowing"

            //If isStatusShowing has a value of false, set the value to true
            //as well as call the function "FindSeatStatus"

            //If isStatusShowing has a value of true, set the value to false
            //and set the TextBox "txtStatus" to ""

            if (planeRow.SelectedIndex.ToString() == "-1" || planeColumn.SelectedIndex.ToString() == "-1")
            {
                MessageBox.Show("You have not chosen either a row or column, please try again!");
            }
            else
            {
                if (!isStatusShowing)
                {
                    isStatusShowing = true;
                    FindSeatStatus();
                }
                else
                {
                    isStatusShowing = false ;
                    txtStatus.Text = "";
                }

                
            }


        }

        //Call a onClick function when the button "btnCancel" gets pressed

        private void btnCancel_Click(object sender, EventArgs e)
        {
            //If the planeRow listbox or planeColumn listBox have a value of -1 (nothing has been selected)
            //Then display a messagebox telling the user to choose a row and column

            //If the user has chosen a valid row and column call the function  "CancelUserSeat" 
            //to cancel the seat taken by a user

            if (planeRow.SelectedIndex.ToString() == "-1" || planeColumn.SelectedIndex.ToString() == "-1")
            {
                MessageBox.Show("You have not chosen either a row or column to cancel, please try again!");
            }
            else
            {
                CancelUserSeat();
            }


        }


        //Call a SelectedIndexChanged when the listBox's "planeColumn" index is changed

        private void planeRow_SelectedIndexChanged(object sender, System.EventArgs e)
        {

            //If the status text box "txtStatus" has a value that is not empty or 
            //has a length that is greater than 0 set the txtStatus text value to ""

            if (txtStatus.Text != "" || txtStatus.Text.Length > 0)
            {
                txtStatus.Text = "";

            }

            //set the Boolean value of isStatusShowing to false
            isStatusShowing = false;
        }

        //Call a SelectedIndexChanged when the listBox's "planeColumn" index is changed

        private void planeColumn_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            //If the status text box "txtStatus" has a value that is not empty or 
            //has a length that is greater than 0 set the txtStatus text value to ""

            if (txtStatus.Text != "" || txtStatus.Text.Length > 0)
            {
                txtStatus.Text = "";
            }

            //set the Boolean value of isStatusShowing to false
            isStatusShowing = false;
        }


        //Call a onClick function when the button "btnShowAll" gets pressed

        private void btnShowAll_Click(object sender, EventArgs e)
        {   
            //If the panel "seatChart" is Visible when the button is clicked then set rich Text
            //box text to be empty

            //If the panel "seatChart" is not Visible call a function to display the panel which
            //contains the buttons(the outline of the plane seats) and the rich text box displaying 
            //The seatst that are filled with customers

            if (seatChart.Visible)
            { rchTxtSeats.Text = ""; }
            else
            { ShowSeatInfo(); }


            //Set the Visibilty of panel, "seatChart", to the opposite of whatever seatChart.Visible value
            //is.
            seatChart.Visible = !seatChart.Visible;
        }

    }
}
